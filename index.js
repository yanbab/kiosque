const {app, BrowserWindow, screen, dialog} = require('electron')
const minimist = require('minimist')
const path = require('path')
const url = require('url')
const RENDERER = 'index.html'
const PRELOAD_SCRIPT = 'preload.js'
let win = null



function toURL(arg) {
  let url = new URL(arg)
  if(!url.protocol) {
    url = new URL(url.fileURLToPath(arg))
  }
  return url.href
}

function getArgv() {
  if(app.isPackaged) {

  }
}


function createWindow() {
  const { width, height } = screen.getPrimaryDisplay().workAreaSize
  let win = new BrowserWindow({
    width,
    height,
    backgroundColor: '#000',
    show: false,
    frame: true,
    resizable: false,
    minimizable: false,
    
    //maximizable: false,
    closable: false,
    webPreferences: {
      // nodeIntegration: true,
      // partition: 'persist:xxx',
      // preload: path.join(__dirname, PRELOAD_SCRIPT)
    }
  })
  win.setFullScreen(true)
  win.removeMenu() // linux, windows
  win.on('closed', function () {
    win = null 
  })
  win.on('ready-to-show', function () {
    win.show()
  })
  return win
}

function ready() {
  
  var args = minimist(process.argv)
  // if(!args.open) {
  //   app.quit()
  //   return
  // }
  win = createWindow()
  win.loadFile(RENDERER)
  //dialog.showMessageBox(win)

}
 
//app.dock.hide() // macos
app.on('ready', ready)

